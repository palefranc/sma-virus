#include "GlobuleBlanc.h"
#include "mt.hpp"
#include "Terrain.h"
#include <iostream>
  
GlobuleBlanc::GlobuleBlanc(int x,int y,int DureeVie ):Entite(x,y,DureeVie){}


 LymphocyteT::LymphocyteT(int x,int y,int DureeVie ):GlobuleBlanc(x,y,DureeVie){}

 void LymphocyteT::Attaquer(int i,int j,vector<vector<int>> & matrice,vector<Cellule> & cellule,vector<Virus> & virus)
  {
     std::cout << "La methode attaquer cellule est appelé ( " << x <<"," << y  << ")" << std::endl;
     matrice[x][y]=0;
     for(unsigned int k=0;k<virus.size();k++)
      {
         if(virus[k].getx()==i && virus[k].gety()==j) 
               virus.erase(virus.begin()+k);
       }
     for(unsigned int k=0;k<cellule.size();k++)
      {
         if(cellule[k].getx()==i && cellule[k].gety()==j) 
                 cellule.erase(cellule.begin()+k);
       }
     x=i;
     y=j;
     matrice[x][y]=1;
   }
 void LymphocyteT::SeDeplacer(vector<vector<int>> & matrice,vector <Cellule> & cellule,vector<Virus> & virus)
  {
     std::cout << "La methode Sedeplacer LymphocyteT est appelé" << std::endl;
     bool trouve=false;
     int i=x-1;
     while(i<=x+1 && !trouve){
        int j=y-1;
        while(j<=y+1 && !trouve){
           if(i>=0 && j>=0 && i<matrice.size() && j<matrice[i].size() && matrice[i][j]==5)  
            {
              trouve=true;
              std::cout << "On a trouver une cellule infecté" << std::endl;
              Attaquer(i,j,matrice,cellule,virus); 
            }
           else
            {
              j++;
             }
         }
         i++;
      } 
      if(!trouve) {
        int k=0;
        bool fin=false;
         while(k<5 && !fin)
         {
           i=rand(x-1,x+1); //CHANGER(8 VOISINS)
           int j=rand(y-1,y+1); //CHANGER(8 VOISINS)
            if(j>=0 && i>=0 && i<matrice.size() && j<matrice[0].size() && matrice[i][j]==0)
             {
                matrice[x][y]=0;
                fin=true;
                x=i;
                y=j;
                matrice[x][y]=1;
              }
            k++;
          }     
      }   
  }


  Macrophage::Macrophage(int x,int y,int DureeVie ):GlobuleBlanc(x,y,DureeVie){}
  
  void Macrophage::Attaquer(int i,int j,vector<vector<int>> & matrice,vector<Virus> & virus)
   {
      matrice[x][y]=0;
      std::cout << "On a attaquer un virus ( " << x <<"," << y  << ")" << std::endl;
     for(unsigned int k=0;k<virus.size();k++)
      {
         if(virus[k].getx()==i && virus[k].gety()==j) virus.erase(virus.begin()+k);
       }
     x=i;
     y=j;
     matrice[x][y]=2;
    std::cout << "On a fini d'attaquer le virus ( " << x <<"," << y  << ")" << std::endl;
    }
  void Macrophage::SeDeplacer(vector<vector<int>> & matrice,vector<Virus> & virus)
   {
     std::cout << "La methode Sedeplacer Macrophage est appelé" << std::endl;
     bool trouve=false;
     int i=x-1;
     while(i<=x+1 && !trouve){
        int j=y-1;
        while(j<=y+1 && !trouve){
           if(i>=0 && j>=0 && i<matrice.size() && j<matrice[i].size() && matrice[i][j]==4) 
            {
              trouve=true;
              std::cout << "On a trouver un virus" << std::endl;
              Attaquer(i,j,matrice,virus); 
            }
           else
            {
              //std::cout << "Pas de virus (" << i << "," << j << ")" << std::endl;
              j++;
             }
         }
        //std::cout << "On incremente i" << std::endl;
        i++;
      } 
     if(!trouve){
        std::cout << "On a pas trouver un virus ( " << x <<"," << y  << ")" << std::endl;
        int k=0;
        bool fin=false;
         while(k<5 && !fin)
         {
           i=rand(x-1,x+1);
           int j=rand(y-1,y+1); 
            if(j>=0 && i>=0 && i<matrice.size() && j<matrice[0].size() && matrice[i][j]==0)
             {
                matrice[x][y]=0;
                fin=true;
                 x=i;
                 y=j;
                 matrice[x][y]=2;
              }
             k++;
          }     
      }    
    }
