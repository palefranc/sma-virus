/**
 * \file main.cpp
 * \brief Ce fichier contient la methode main qui est l'entré du systeme.
 * \author Asmaa.Hermak 
 * \version 0.1
 * \date 4 mars 2019
 *
 *
 */
#include "Terrain.h"
#include <iostream>
/**
 * \fn int main (void)
 * \brief Entrée du programme.
 *
 * \return 0 - Arrêt normal du programme.
 */
int main(){
        unsigned long init[4]={0x123, 0x234, 0x345, 0x456}, length=4;
        init_by_array(init, length);
        int choix=0;
	while(choix!=2)
	{
		std::cout << "1 : Cliquer 1 pour demarer une simulation" << std::endl;
		std::cout << "1 : Cliquer 2 pour  sortir de l'application" << std::endl;
		std::cin >> choix ;
                if(choix!=1 && choix !=2)
                     std::cout << "Choisir le numero 1 ou 2" << std::endl;
                else if(choix==1)
                {
                   int t=0,c=0,v=0,n=0;
  		   std::cout << "Donner la taille du terrain: " << std::endl;
                   std::cin >> t ;
		   std::cout << "Donnner le nombre du cellule dans le terrain: " << std::endl;
                   std::cin >> c ;
		   std::cout << "Donner le nombre des virus dans le terrain: " << std::endl;
                   std::cin >> v ;
                   std::cout << "Donner le nombre d'iterations: " << std::endl;
                   std::cin >> n ;
                   
                   Terrain::getInstance()->init(c,v,t,t);
                   for(int i=0;i<n;i++){
                       Terrain::getInstance()->afficher();
                       Terrain::getInstance()->AvancerTemps();
					}
                
                }
	}

 return 0;
}
