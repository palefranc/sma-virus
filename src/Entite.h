/**
 * \file Entite.h
 * \brief Ce fichier Contient la classe Entite.
 * \author Asmaa.Hermak
 * \version 0.1
 * \date 5 Janvier 2019
 *
 *
 */
#ifndef ENTITE_HPP
#define ENTITE_HPP
#define Vision 8

#include "mt.hpp"

  /** 
   * \class Entite
   * \brief classe qui represente les entités, une entité peut etre (un virus ,cellule ou globule blanc)
   *
   */
class Entite {
 protected:
   int x;
   int y;
   int dureeVie;
 public:
    /**  
     *
     *  \brief Constructeur de la classe Entite
     *  \param x : abscisse de l'entité dans le terrain
     *  \param y : l'ordonnée de l'entité dans le terrain
     *  \param DurreVie : duree de vie de l'entité,nombre d'iteration que l'entité doit exister dans le terrain avant de disparaître
     */
    Entite(int x,int y,int DurreVie);
    /**  
     * \brief  getter,permet de recuprer l'abscisse de l'entité
     * \return retourne l'abscisse de l'entité
     */
    int getx();
    /**  
     * \brief getter,permet de recuprer l'ordonné de l'entité
     * \return retourne l'ordonné de l'entité
     */
    int gety();
    /**  
     * \brief getter,permet de recuprer la duree de vie de l'entité, nombre d'itération qu'elle peut exister dans le terrain
     * \return retourne la duree de vie de l'entité
     */
    int getdureeVie();
    /**  
     * \brief setter,permet de fixer l'abscisse de l'entité.
     */
    void setx(int xarg);
    /** 
     *  \brief setter,permet de fixer l'ordonnée de l'entité.
     */
    void sety(int yarg);
    /**  
     * \brief setter,permet de fixer la duree de vie de l'entité, nombre d'itération qu'elle peut exister dans le terrain
     */
    void setdureeVie(int dureeVieArg);
    /** 
     *  \brief setter,permet de diminuer de 1 la duree de vie da la cellule.
     */
    void DiminuerVie();
    /** 
     * \brief permet de savoir si l'entité doit mourir ,cad disparaitre du terrain ou pas
     * \return retourne true si l'entité doit disparaitre du terrain et false sinon
     */
    bool Mourir();

};

    /**  
     * \brief génére un nombre aléatoire dans l'intervalle passer en parametre 
     * \param a : la borne inferieure de l'intervalle 
     * \param b : la borne superieur de l'intervalle
     * \return retourne un nombre entre a et b
     */

int rand(int a,int b);
#endif
