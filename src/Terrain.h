#ifndef TERRAIN_HPP
#define TERRAIN_HPP
/**
 * \file Terrain.h
 * \brief Ce fichier contientla classe terrain.
 * \author Asmaa.Hermak Pierre-Antoine.Lefranc
 * \version 0.1
 * \date 3 Fevrier 2019
 *
 *
 */
#include "Virus.h"
#include "Cellule.h"
#include "GlobuleBlanc.h"
#include <vector>
using namespace std;
  /** 
   * \class Terrain
   * \brief classe representant les Terrain, c'est lui qui va contenir tous les entités (Virus ,globule blanc ou cellule)
   */
class Terrain{
 private:
	static Terrain* terrain;
   vector<vector<int>> matrice;
   vector<Virus> virus;
   vector<Cellule> cellule;
   vector<Macrophage> macro;
   vector<LymphocyteT> lympho;
   int largeur;
   int hauteur;
   //int Timer;
   
   Terrain();
   
 public:
    /**  
     *  \brief Cette methode permet d'initialiser le terrain avec les valeurs passé en parametre
     *  \param nbrCellule : nombre de cellule à mettre dans le terrain
     *  \param nbrVirus : nombre de virus à mettre dans le terrain
     *  \param l : langueur du terrain
     *  \param h : hauteur du terrain
     */
  void init(int nbrCellule,int nbrVirus,int l,int h);
    /**  
     *\brief Cette methode permet de vider le terrain de toute les entités
     *
     */
  void clear();
    /**  
     * Cette methode permet d'avancer le temps ,ce qui veut dire deplacer tous les entités dans le terrain,et appeler les 
     *  methodes d'attaque,de reproduction (pour les cellules ) et de multiplication (pour les virus) si necessaire. 
     *
     */
  void AvancerTemps();
    /** 
     * Cette methode permet d'ajouter des globules blancs dans le terrain, ces globules blancs peuvent etre des LymphocyteT ou
     *  Macrophage selon le parametre type passé en argument.
     *
     *  \param type : c'est le type de globule blancs qu'on veut ajouter, 1 pour les LymphocyteT et 2 pour les Macrophage. 
     */
  void AjouterGlobulesBlanc(int type);
    /** 
     *  \brief Cette methode permet de recuperer la hauteur du terrain
     *  \return retourne l'hauteur du terrain
     */
  int getHauteur();
    /** 
     *  \brief Cette methode permet de recuperer la largeur du terrain
     *  \return retourne la largeur du terrain
     */
  int getLargeur();
    /** 
     *  \brief Cette methode permet d'afficher le terrain et ses composants (les entité) dans la console
     *
     */
  void afficher();
    /**  
     *  \brief Cette methode permet de recuperer l'unique instance du terrain
     *  \return Retourne un pointeur qui pointe sur une instance du terrain
     */
  static Terrain* getInstance();
    /**  
     *  \brief Cette methode permet de recuperer la matrice qui represente le terrain avec ses composants
     *  \return Retourne une matrice d'entier ,chaque entier est un type d'entité
     */
  vector<vector<int>> getTerrain();
    /**  
     *  \brief Cette methode permet de recuperer une ligne de la matrice qui represente le terrain
     *  \param i : l'indice de la ligne du terrain qu'on veut recuperer
     *  \return retourne une liste d'entiers qui est une ligne du terrain
     */
  vector<int> operator[](int i);
};
#endif

