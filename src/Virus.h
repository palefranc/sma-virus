#ifndef VIRUS_HPP
#define VIRUS_HPP
/**
 * \file Virus.h
 * \brief Ce fichier contient la classe virus.
 * \author Pierre-Antoine.Lefranc
 * \version 0.1
 * \date 5 Janvier 2019
 *
 */
#include "Cellule.h"
#include "Entite.h"
#include <vector>

using namespace std;

class Terrain;
  /**
   * \class Virus 	
   * \brief classe qui represente les Virus
   */
class Virus :public Entite{
	
	public:
    /**  
     *  \brief Constructeur de la classe Virus
     *  \param x : abscisse du Virus dans le terrain
     *  \param y : l'ordonnée du Virus dans le terrain
     *  \param d : duree de vie du Virus ,nombre d'iteration que le virus va exister dans le terrain avant de disparaitre
     */
		Virus(int x,int y,int d);
    /**  
     *   Une methode qui permet le deplacement du virus qui n'a pas encore affecté une cellule dans le terrain, s'il trouve une 
     *  cellule non inffecté dans son champ de vision il va l'attaquer et prendre sa place,sinon il va se deplacer aleatoirement
     *
     *  \param matrice : la matrice qui represente le terrain
     *  \param cellule : Liste des cellules existant dans le terrain
     *  \param hauteur : hauteur du terrain
     *  \param largeur : largeur du terrain
     */
		void SeDeplacer(vector<vector<int> > & matrice, vector<Cellule> & cellule,int hauteur,int largeur);
    /**  
     *  Cette methode va permetre le Virus à attaquer une cellule et l'infecter.
     *  \param cellule : la liste des cellules presentent dans le terrain
     *
     */
		void Infecter(vector<Cellule> & cellule);
    /**  
     *  Cette methode permet à un virus de se multiplier,detruire la cellule qu'il a infecté et produire d'autre virus
     *
     *  \param n : le nombre de virus à produire
     *  \param matrice : la matrice qui represente le terrain
     *  \param virus : liste des virus existant dans le terrain
     */

		void SeMultiplier(int n, vector<vector<int> > & matrice, vector<Virus> & virus);
    /**  
     *  Cette methode permet de comparer les positions de deux virus
     *  \param v : le virus qu'on doit comparer sa position avec la position du virus courant
     *  \return true si les deux virus sont en meme emplacement et false sinon
     */
		
		bool operator==(Virus& v);
};

#endif


