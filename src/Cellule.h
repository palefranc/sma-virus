/**
 * \file Cellule.h
 * \brief On trouve dans ce fichier la classe Cellule
 * \author Di.Wang
 * \version 0.1
 * \date 5 Janvier 2019
 *
 *
 */
#ifndef CELLULE_HPP
#define CELLULE_HPP
#include "Entite.h"
#include <vector>
using namespace std;
  /** 
   * \class Cellule
   * \brief classe represente les cellules, elle herite de la classe Entite
   *
   */
class Cellule : public Entite {
 private:
  /** 
   * \property Inffecte
   * \brief La valeur de cette propriété va etre vrai si la cellule est inffecté
   *
   */
   bool Inffecte;
  /** 
   * \property delaisVirus
   * Cette propriete represente le delais que le virus doit attendre avant de detruire le cellule et se multiplier ( si la          
   * cellule est infecté par ce virus )
   *
   */
   int delaisVirus;
  /** 
   * \property delais
   * \brief Cette proprièté represente le delais que la cellule doit attendre avant de se multiplier 
   *
   */
   int delais;
 public:
    /**  
     *  \brief Constructeur de la classe cellule
     *  \param x : abscisse de la cellule dans le terrain
     *  \param y : l'ordonnée de la cellule dans le terrain
     *  \param dureeVie : duree de vie de la cellule ,nombre d'iteration que la cellule va exister dans le terrain
     */
  Cellule(int x,int y,int dureeVie);
    /**   
     *  Cette methode permet à la cellule de se reproduire ,c'est à dire crée une autre cellule et la positionner aleatoirement 
     *  dans son champ de vision.
     *
     *  \param matrice : la matrice qui represente le terrain et ses composants
     *  \param cellule : liste des cellules existant dans le terrain
     *  \param hauteur : un entier qui represente la hauteur du terrain
     *  \param largeur : un entier qui represente la largeur du terrain
     */
  void SeReproduire(vector<vector<int>> & matrice,vector<Cellule> & cellule,int hauteur ,int largeur);
    /**  
     *  Cette methode permet à un virus d'infecter une cellule 
     */
  void Inffecter();
    /** 
     * Cette methode permet de savoir si une cellule est inffecté ou non 
     * \return true si la cellule est infecté et false sinon
     */
  bool EstInffecter();
    /** 
     * Cette methode permet de savoir si la cellule doit se reproduire ou non 
     * \return true si la cellule doit ce reproduire et false sinon
     */
  bool IsSeReproduire();
    /** 
     *  Cette methode permet de savoir si une cellule doit etre detruit pour liberer des virus ou non
     * \return true si la cellule doit liberer des virus et se detruire , et false sinon
     */
  bool FinDelais();

};

#endif

