
#include "Cellule.h"
#include "Terrain.h"
#include <iostream>

Cellule::Cellule(int x,int y,int dureeVie):Entite(x,y,dureeVie){
	delaisVirus=0;
    delais=rand(0,7);
    //std::cout << "Durée vie : " << dureeVie << std::endl;
	Inffecte=false;	
}


void Cellule::SeReproduire(vector<vector<int>> & matrice,vector<Cellule> & cellule,int hauteur ,int largeur){
  //std::cout<< "bonjour dans la fonction se reproduire" <<std::endl;
  bool cree=false;
  int i=-1;
  while(i<2 && cree==false){
      int j=-1; 
      while(j<2 && cree==false){
        if(x+i<(int)largeur && x+i>=(int)0 && y+j<hauteur && y+j>=(int)0 ){
			if( matrice[x+i][y+j]==0){
           Cellule c(x+i,y+j,rand(0,10));
           matrice[x+i][y+j]=3;
           cellule.push_back(c);
           //std::cout<< "une cellule est ajouté" <<std::endl;
           cree=true;
	      }
        }
        j++;
      }
    i++;
  }
}
void Cellule::Inffecter(){
 delaisVirus=rand(0,4);
 Inffecte=true;
}
bool Cellule::EstInffecter(){
  return Inffecte;
}

bool Cellule::IsSeReproduire(){
  if(delais==0 )
   {
     delais=rand(0,7);
     return true;
   }
  else 
   {
    delais--;
    return false;
   }
}

bool Cellule::FinDelais(){
  if(delaisVirus==0)
    return true;
  else
   {
    delaisVirus--;
    return false;
   }
}

