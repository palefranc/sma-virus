#include "Virus.h"
#include "mt.hpp"
#include <climits>
#include <vector>
#include <iostream>

using namespace std;

Virus::Virus(int x, int y, int d): Entite(x, y, d)
{
}

void Virus::SeDeplacer(vector<vector<int> > & matrice, vector<Cellule> & cellule,int hauteur ,int largeur)
{
	bool celtrouve = false;
        int i=-1;	
      if(matrice[x][y]==4){
        while(i<2 && celtrouve==false){
            int j=-1; 
            while(j<2 && celtrouve==false){
                if(x+i<largeur && x+i>=0 && y+i<hauteur && y+i>=(int)0 && matrice[x+i][y+j]==3){
                     matrice[x][y]=0;
                     x=x+i;
                     y=y+j;                      
                     matrice[x][y]=5;
                     Infecter(cellule);
                     std::cout<< "le virus a affecté" <<std::endl;
                     celtrouve=true;
                 }
                 j++;
             }
             i++;
        }
      }
        int k=0,j=0;
	bool fin =false;
	if(!celtrouve)
	{
              while(k<5 && !fin)
              {
                   i=rand(x-1,x+1);
                   j=rand(y-1,y+1);
                   if(j>=0 && i>=0 && i<matrice.size() && j<matrice[0].size()  && matrice[i][j]==0)
                   {
                        matrice[x][y]=0;
                        fin=true;
                        x=i;
                        y=j;
                        matrice[x][y]=4;
                        //std::cout<< "le virus se deplace aleatoirement" <<std::endl;
                    }
                    k++;
                }
         }     
	
}

void Virus::Infecter(vector<Cellule> & c)
{
	
	for(unsigned i=0; i<c.size(); i++)
	{
		if ((c[i].getx()==x) && (c[i].gety()==y))
			c[i].Inffecter();
	}
}

void Virus::SeMultiplier(int n, vector<vector<int> >& t, vector<Virus>& v)
{
    //std::cout <<"La fonction se multiplier estb appelé"<< std::endl;
    
    int i, j;
    int compt=5;
    int rang = 1;
    int taille = t.size();
    
    while(n>0)
    {
		do {
			i = x + rand(-rang, rang);
			j = y + rand(-rang, rang);
			compt--;
			if(compt<0)
			{
				compt=5;
				rang++;
			}
		}while((i<0) || (j<0) || (i>=taille) || (j>=taille) ||
				(t[i][j]!=0));
				
		compt=5;
		rang=1;
		v.push_back(Virus(i, j, rand(0, 10)));
                //std::cout <<"Un virus est ajouté"<< std::endl;
		t[i][j] = 4;
		n--;
	}
}

bool Virus::operator==(Virus& v)
{
	return((this->x==v.x) && (this->y==v.y));
}
