/**
 * \file GlobuleBlanc.h
 * \brief Ce fichier contient la classe GlobuleBlanc et tous les classes qui heritent de cette classe (LymphocyteT et Macrophage)
 * \author Asmaa.Hermak
 * \version 0.1
 * \date 5 Janvier 2019
 *
 */
#ifndef GLOBULE_HPP
#define GLOBULE_HPP
#include "Virus.h"
#include "Cellule.h"
#include "Entite.h"
#include <vector>

using namespace std;

class Terrain;
  /** 
   * \class GlobuleBlanc
   * \brief classe qui represente les GlobuleBlanc
   *
   */
class GlobuleBlanc :public Entite {
 public:
  /**  
   * \brief Constructeur de la classe GlobuleBlanc
   * \param x : abscisse de la globule blanc dans le terrain
   * \param y : l'ordonnée de la globule blanc dans le terrain
   * \param DureeVie:duree de vie de la globule blanc ,nombre d'iteration que l'entite va exister dans le terrain avant de disparaitre
   */
  GlobuleBlanc(int x,int y,int DureeVie );
  //virtual void Attaquer(int i,int j,vector<vector<int>> & matrice)=0;
  //virtual void SeDeplacer(vector<vector<int>> & matrice,vector<Cellule> & cellule,vector<Virus> & virus)=0;
};

  /** 
   * \class LymphocyteT
   * Classe representant les LymphocyteT, un type de globules blanc qui attaque les cellules inffecté, elle herite de 
   * GlobuleBlanc 
   *
   */
class LymphocyteT :public GlobuleBlanc {
 public:
  /**  
   *  \brief Constructeur de la classe LymphocyteT
   *  \param x : abscisse du LymphocyteT dans le terrain
   *  \param y : l'ordonnée du LymphocyteT dans le terrain
   *  \param DureeVie : duree de vie du LymphocyteT,nombre d'iteration que l'LymphocyteT va exister dans le terrain.
   */
  LymphocyteT(int x,int y,int DureeVie );
    /** 
     *  Cette methode va permetre le LymphocyteT à attaquer la cellule inffecté ,la detruire avec le virus qui l'a inffecté et 
     *  prendre sa place dans le terrain
     *
     *  \param matrice : la matrice qui represente le terrain
     *  \param cellule : Liste des cellules existant dans le terrain
     *  \param virus : Liste des virus existant dans le terrain
     *  \param i : l'abscisse de la cellule infecté qu'elle doit attaquer
     *  \param j : l'ordonnée de la cellule inffecté qu'elle doit attaquer
     */
  virtual void Attaquer(int i,int j,vector<vector<int>> & matrice,vector<Cellule> & cellule,vector<Virus> & virus);
    /**  
     *  Une methode qui permet le deplacement du lymphocyte dans le terrain, s'elle trouve une cellule inffecté dans son champ
     *  de vision elle va l'attaquer et prendre sa place,sinon elle va se deplacer aleatoirement
     *
     *  \param matrice : la matrice qui represente le terrain
     *  \param cellule : Liste des cellules existant dans le terrain
     *  \param virus : Liste des virus existant dans le terrain
     */
  virtual void SeDeplacer(vector<vector<int>> & matrice,vector <Cellule> & cellule,vector<Virus> & virus);
};
  /** 
   * \class Macrophage
   * \brief classe representant les Macrophage, un type de globules blanc qui attaque les virus, elle herite de GlobuleBlanc 
   *
   */
class Macrophage :public GlobuleBlanc {
 public:
    /**  
     *  \brief Constructeur de la classe Macrophage
     *  \param x : abscisse d'un objet Macrophage dans le terrain
     *  \param y : l'ordonnée d'un objet Macrophage dans le terrain
     *  \param DureeVie : duree de vie d'un objet Macrophage,nombre d'iteration que l'entite va exister dans le terrain
     */
  Macrophage(int x,int y,int DureeVie );
    /**  
     * Cette methode va permetre à un objet Macrophage à attaquer le virus,le detruire et prendre sa place dans le terrain
     *
     *  \param matrice : la matrice qui represente le terrain
     *  \param virus : liste des virus existant dans le terrain
     *  \param i : l'abscisse du virus qu'elle doit attaquer
     *  \param j : l'ordonnée du virus qu'elle doit attaquer
     */
  virtual void Attaquer(int i,int j,vector<vector<int>> & matrice,vector<Virus> & virus);
    /**  
     * Une methode qui permet le deplacement du Macrophage dans le terrain, s'elle trouve un virus dans son champ de vision 
     *  elle va l'attaquer et prendre sa place,sinon elle va se deplacer aleatoirement
     *
     *  \param matrice : la matrice qui represente le terrain
     *  \param virus : liste des virus existant dans le terrain
     */
  virtual void SeDeplacer(vector<vector<int>> & matrice,vector<Virus> & virus);
};


#endif

