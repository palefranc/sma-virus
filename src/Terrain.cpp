#include "Terrain.h"
#include <iostream>

using namespace std;

Terrain::Terrain(){
}

Terrain* Terrain::getInstance()
{
	if (terrain==nullptr)
		terrain = new Terrain();
	return(terrain);
}

Terrain* Terrain::terrain=nullptr;

void Terrain::init(int nbrCellule,int nbrVirus,int l,int h) {
	this->clear();
	
  int x,y,dureevie;
  largeur = l;
  hauteur = h;
  matrice.resize(largeur);
  for(unsigned i=0;i<matrice.size();i++){
    matrice[i].resize(hauteur);
  }
  
  for(int i=0;i<nbrCellule;i++)
  {
    x=rand(0,largeur);
    y=rand(0,hauteur);
    dureevie=rand(0,10);
    cellule.push_back(Cellule(x,y,dureevie));
    matrice[x][y]=3;
   }

 
  for(int i=0;i<nbrVirus;i++)
  {
    x=rand(0,largeur);
    y=rand(0,hauteur);
    dureevie=rand(0,10);
    if(matrice[x][y]==0){
      matrice[x][y]=4;
      virus.push_back(Virus(x,y,dureevie));
    }
    else if(matrice[x][y]==3){
      matrice[x][y]=5;
      virus.push_back(Virus(x,y,dureevie));
    }  
   }
 
}

void Terrain::clear()
{
	matrice.clear();
	virus.clear();
	cellule.clear();
	macro.clear();
	lympho.clear();
}

vector<vector<int>> Terrain::getTerrain()
{
	return(matrice);
}

void Terrain::afficher()
{
	int hauteur = matrice.size();
	int largeur = matrice[0].size();
	
	cout << "+";
	for(int i=0; i<largeur; i++)
		cout << "-";
	cout << "+" << endl;
	
	for(int j=0; j<hauteur; j++)
	{
		cout << "|";
		for(int i=0; i<largeur; i++)
		{
			switch(matrice[i][j]){
				case(0) : cout << "." ;
					break;
				case(1) : cout << "L" <<"(" << i <<"," << j <<")";
					break;
				case(2) : cout << "M" <<"(" << i <<"," << j <<")";
					break;
				case(3) : cout << "C"<<"(" << i <<"," << j <<")";
					break;
				case(4) : cout << "V"<<"(" << i <<"," << j <<")";
					break;
				case(5) : cout << "A"<<"(" << i <<"," << j <<")";
			}
                        if(i!=largeur-1) cout << "-";
		}
		cout << "|" << endl;
	}
	
	cout << "+";
	for(int i=0; i<largeur; i++)
		cout << "-";
	cout << "+" << endl;
}

vector<int> Terrain::operator[](int i)
{
	return(matrice[i]);
}

void Terrain::AvancerTemps(){
  std::cout << "le nombre de cellules est" << cellule.size() << std::endl;
 unsigned i=0;
  while (i<cellule.size())
  {
    unsigned taille= cellule.size();
          //std::cout << "On va commencer avec les cellule" << i << std::endl;
          if(cellule[i].IsSeReproduire() && !cellule[i].Mourir() && !cellule[i].EstInffecter())
          {
                    //std::cout << "la cellule " << i << " doit se reproduire"<< std::endl;
//std::cout << "la cellule " << i << "("<< cellule[i].getx() << ","<< cellule[i].gety() << ")" << "valeur"<< matrice[cellule[i].getx()][cellule[i].gety()] << std::endl;
                    cellule[i].SeReproduire(matrice,cellule,hauteur,largeur); 
                    cellule[i].DiminuerVie();
                    //std::cout << "la cellule " << i << " finit de se reproduire"<< std::endl;
                    //std::cout << "la taille des cellules " << cellule.size() << std::endl;                  
           }
          else if(cellule[i].Mourir() && cellule[i].EstInffecter()==true)
          {
                    //std::cout << "la cellule " << i << " doit mourir"<< std::endl;
                    unsigned j=0;
                    while(j<virus.size())
                    {
						unsigned taille2= virus.size();
                         if(virus[j].getx()==cellule[i].getx() && virus[j].gety()==cellule[i].gety() )
                          {
                                virus.erase (virus.begin()+j);
                          }
                          if(taille2<=virus.size()) j++;
                     }
                    matrice[cellule[i].getx()][cellule[i].gety()]=0;
                    cellule.erase (cellule.begin()+i);
                    //std::cout << "la cellule " << i << " est meurt"<< std::endl;
                    //std::cout << "la taille des cellules " << cellule.size() << std::endl;                  
          }
          else if(cellule[i].Mourir())
          {
                   // std::cout << "la cellule " << i << " doit mourir seule"<< std::endl;
                    matrice[cellule[i].getx()][cellule[i].gety()]=0;
                    cellule.erase (cellule.begin()+i);
                   // std::cout << "la cellule " << i << " est meurt seule"<< std::endl;
                   // std::cout << "la taille des cellules " << cellule.size() << std::endl;                  
           }
          else if(cellule[i].EstInffecter() && cellule[i].FinDelais() )
          {
                  //  std::cout << "la cellule " << i << " produit des virus"<< std::endl;
                     for(unsigned j=0;j<virus.size();j++)
                     {
                             if(virus[j].getx()==cellule[i].getx() && virus[j].gety()==cellule[i].gety() )
                                    virus[j].SeMultiplier(rand(0,5),matrice,virus);
                      }
                      matrice[cellule[i].getx()][cellule[i].gety()]=4;
                      cellule.erase (cellule.begin()+i);
                  //  std::cout << "la cellule " << i << " finit de produir des virus"<< std::endl;
     
            }
          else
          {
                  //   std::cout << "la cellule " << i << " continu à exister"<< std::endl;
                      cellule[i].DiminuerVie();
                  //  std::cout << "la cellule " << i << " a diminuer sa vie"<< std::endl;
           }
      if(taille<=cellule.size()) i++;
   }
   //std::cout << "la taille des cellules " << cellule.size() << std::endl;

  i=0;
  //std::cout << "la taille des virus " << virus.size() << std::endl;
  while (i<virus.size())
  {
         // std::cout << "On va commencer avec les virus" << i << std::endl;
          unsigned taille= virus.size();
          if(virus[i].Mourir() && matrice[virus[i].getx()][virus[i].gety()]==5 )
          {
                   //  std::cout << "le virus " << i << " doit mourir avec la cellule affecté"<< std::endl;
                   unsigned j=0;
                     while(j<cellule.size())
                     {
						 unsigned taille2= cellule.size();
                         if(cellule[j].getx()==virus[i].getx() && cellule[j].gety()==virus[i].gety())
                                cellule.erase (cellule.begin()+j);
                         if(taille2<=cellule.size()) j++;
                      }
                     matrice[virus[i].getx()][virus[i].gety()]=0;
                     virus.erase (virus.begin()+i);
                   //  std::cout << "la cellule " << i << " est meurt avec la cellule"<< std::endl;
           }
          else if(virus[i].Mourir())
          {
                     std::cout << "le virus (" << virus[i].getx() << ", " << virus[i].gety() << ") doit mourir seul "<< std::endl;
                     matrice[virus[i].getx()][virus[i].gety()]=0;
                     virus.erase (virus.begin()+i);
                  //   std::cout << "la cellule " << i << " est meurt seul"<< std::endl;
           }
          else if(matrice[virus[i].getx()][virus[i].gety()]==5)
          {
                  //   std::cout << "le virus" << i << " continu à exister avec la cellule affecté"<< std::endl;
                     virus[i].DiminuerVie();
                  //   std::cout << "le virus " << i << " est existant avec la cellule affecté"<< std::endl; 
           }
          else
          {
					 std::cout << "le virus (" << virus[i].getx() << ", " << virus[i].gety() << ") continu à exister et se deplace"<< std::endl;
                     virus[i].SeDeplacer(matrice, cellule,hauteur ,largeur);
                     virus[i].DiminuerVie();
                  //   std::cout << "la cellule " << i << " continu à exister et se deplace"<< std::endl; 
           }
      if(taille<=virus.size()) i++;
    }
   //std::cout << "la taille des virus " << virus.size() << std::endl;

    //std::cout << "size macro " << macro.size() << std::endl;
    i=0;
    while(i<macro.size())
    {
         if(macro[i].Mourir())
          {
                    //std::cout << "une macro va mourir " << macro[i].getx() << " , "<< macro[i].gety()  << std::endl;
                    matrice[macro[i].getx()][macro[i].gety()]=0;
                    macro.erase (macro.begin()+i);
           }
          else
          {
                   //std::cout << "une macro va se deplacer "<<  macro[i].getx() << " , "<< macro[i].gety()  << std::endl;
                   macro[i].SeDeplacer(matrice,virus);
                   macro[i].DiminuerVie(); 
                   i++;
          }
     }
  
  //std::cout << "size lympho " << lympho.size() << std::endl; 
  i=0;
  while(i<lympho.size())
   {
          if(lympho[i].Mourir())
           {
                   //std::cout << "une lympho va mourir " << std::endl;
                   matrice[lympho[i].getx()][lympho[i].gety()]=0;
                   lympho.erase (lympho.begin()+i);
            }
          else
           {
                   //std::cout << "une lympho va se deplacer " << std::endl;
                   lympho[i].SeDeplacer(matrice, cellule, virus);
                   lympho[i].DiminuerVie(); 
                   i++;
            }
    }  
  
  
   if(virus.size()>2*macro.size())
    {
       //std::cout << "size virus " << virus.size() << std::endl;
       //std::cout << "size macro " << macro.size() << std::endl;
       //std::cout << "On va ajouter des macro " << std::endl;
       AjouterGlobulesBlanc(2);
     }
   unsigned nb=0;
   for(unsigned i=0;i<matrice.size();i++){
      for(unsigned j=0;j<matrice[i].size();j++){
           if(matrice[i][j]==5) nb++;
       }
   }
   //std::cout << "la taille des macro " << macro.size() << std::endl;
   if(nb>2*lympho.size()) 
    {
       //std::cout << "size nb " << nb<< std::endl;
       //std::cout << "size lympho " << lympho.size() << std::endl;
       //std::cout << "On va ajouter des lympho " << std::endl;
      AjouterGlobulesBlanc(1);
    }
    //std::cout << "la taille des lympho " << lympho.size() << std::endl;
}

void Terrain::AjouterGlobulesBlanc(int type)
{
   int nbr=virus.size()/2;
   int dureevie,x,y;
   if(type==2)
   {
     for(int i=0;i<nbr;i++)
      {
           dureevie=rand(0,10);
           x=rand(0,largeur);
           y=rand(0,hauteur);
           Macrophage l(x,y,dureevie);
           if(matrice[x][y]==4)
            {
              matrice[x][y]=2;
              macro.push_back(l);
              for(unsigned j=0;j<virus.size();j++)
               {
                  if(virus[j].getx()==x && virus[j].gety()==y)
                   {
                      virus.erase (virus.begin()+j);
                    }
                }
             }
           else if(matrice[x][y]==0)
            {
              matrice[x][y]=2;
              macro.push_back(l);
            }
       }
    }
   else
   {
         for(int i=0;i<nbr;i++)
          {
              dureevie=rand(0,10);
              x=rand(0,largeur);
              y=rand(0,hauteur);
              LymphocyteT m(x,y,dureevie);
              if(matrice[x][y]==5)
              {
                  matrice[x][y]=1;
                  lympho.push_back(m);
                  for(unsigned j=0;j<virus.size();j++)
                  {
                      if(virus[j].getx()==x && virus[j].gety()==y)
                      {
                           virus.erase (virus.begin()+j);
                       }
                   }
                  for(unsigned j=0;j<cellule.size();j++)
                  {
                      if(cellule[j].getx()==x && cellule[j].gety()==y)
                      {
                           cellule.erase (cellule.begin()+j);
                       }
                   }
               }
              else if(matrice[x][y]==0)
              {
                   matrice[x][y]=1;
                   lympho.push_back(m);
              }
           }
       }
}
int Terrain::getHauteur(){
   return hauteur;
}
int Terrain::getLargeur(){
   return largeur;
}





