#include"Terrain.h"



int rand(int a,int b){
  return genrand_int31()%(b-a)+a;
}

Terrain::Terrain(int nbrCellule,int nbrVirus,int largeur,int hauteur):largeur(largeur),hauteur(hauteur){
  int x,y,dureevie;
  matrice.resize(hauteur);
  for(int i=0,i<matrice.size();i++){
    matrice[i].resize(largeur);
  }
  
  for(int i=0;i<nbrCellule;i++)
  {
    x=rand(0,largeur);
    y=rand(0,hauteur);
    dureevie=rand(0,10);
    cellule.push_back(Cellule(x,y,dureevie));
    matrice[x][y]=3;
   }
  for(int i=0;i<nbrVirus;i++)
  {
    x=rand(0,largeur);
    y=rand(0,hauteur);
    dureevie=rand(0,10);
    if(matrice[x][y]==0){
      matrice[x][y]=4;
      virus.push_back(Virus(x,y,dureevie));
    }
    else if(matrice[x][y]==3){
      matrice[x][y]=5;
      virus.push_back(Virus(x,y,dureevie));
    }  
   }
}

void Terrain::AvancerTemps(){
 for(int i=0;i<cellule.size();i++){
   if(cellule[i].IsSeReproduire() && cellule[i].Mourir()==false && cellule[i].EstAffecter()==false)
    {
      cellule[i].SeReproduire(); 
      cellule[i].DiminuerVie();
    }
   else if(cellule[i].Mourir() && cellule[i].EstAffecter()==true)
   {
      for(int j=0;j<virus.size();j++)
     {
        if(virus[j].getx()==cellule[i].getx() && virus[j].gety()==cellule[i].gety() )
         {
            virus.erase (virus.begin()+j);
          }
      }
     matrice[cellule[i].getx()][cellule[i].gety()]=0;
     cellule.erase (cellule.begin()+i);
   }
   else if(cellule[i].Mourir())
   {
      matrice[cellule[i].getx()][cellule[i].gety()]=0;
      cellule.erase (cellule.begin()+i);
   }
   else if(cellule[i].EstAffecter() && cellule[i].FinDelais() ){
     for(int j=0;j<virus.size();j++)
     {
        if(virus[j].getx()==cellule[i].getx() && virus[j].gety()==cellule[i].gety() )
            virus[j].SeMultiplier();
      }
     matrice[cellule[i].getx()][cellule[i].gety()]=4;
     cellule.erase (cellule.begin()+i);
     
   }
   else{
      cellule[i].DiminuerVie();
   }
 }

 for(int i=0;i<virus.size();i++)
 {
   if(virus[i].Mourir() && matrice[virus[i].getx()][virus[i].gety()]==5 )
    {
       for(int j=0;j<cellule.size();j++)
        {
          if(cellule[j].getx()==virus[i].getx() && cellule[j].gety()==virus[i].gety())
                  cellule.erase (cellule.begin()+i);
         }
        virus.erase (virus.begin()+i);
        matrice[virus[i].getx()][virus[i].gety()]=0;
    }
   else if(virus[i].Mourir())
    {
        virus.erase (virus.begin()+i);
        matrice[virus[i].getx()][virus[i].gety()]=0;
     }
   else if(matrice[virus[i].getx()][virus[i].gety()]==5)
     {
        virus[i].DiminuerVie(); 
     }
   else
     {
        virus[i].Sedeplacer();
        virus[i].DiminuerVie(); 
     }
  }

 for(int i=0;i<gb.size();i++)
 {
   if(gb[i].Mourir())
    {
      gb.erase (gb.begin()+i);
      matrice[gb[i].getx()][gb[i].gety()]=0;
    }
   else
    {
      gb[i].Sedeplacer();
      gb[i].DiminuerVie(); 
    }
  }  

 if(virus.size()>2*gb.size())
      AjouterGlobulesBlanc(1);
 int nb=0;
 for(int i=0;i<matrice.size();i++){
    for(int j=0;j<matrice[i].size();j++){
       if(matrice[i]==5) nb++;
    }
 }
 if(nb>2*gb.size())
      AjouterGlobulesBlanc(2);
  
}

void Terrain::AjouterGlobulesBlanc(int type){
  int nbr=rand(0,virus.size()/2);
  int dureevie,x,y;
   if(type==1)
   {
     for(int i=0,i<nbr;i++)
      {
           dureevie=rand(0,10);
           x=rand(0,largeur);
           y=rand(0,hauteur);
           if(matrice[x][y]==4)
            {
              matrice[x][y]=1;
              gb.push_back(LymphocyteT(x,y,dureevie));
              for(int j=0;j<virus.size();j++)
               {
                  if(virus[j].getx()==x && virus[j].gety()==y)
                   {
                      virus.erase (virus.begin()+j);
                    }
                }
             }
           else if(matrice[x][y]==0)
            {
              matrice[x][y]=1;
              gb.push_back(LymphocyteT(x,y,dureevie));
            }
       }
    }
   else
   {
     for(int i=0,i<nbr;i++)
      {
          dureevie=rand(0,10);
          x=rand(0,largeur);
          y=rand(0,hauteur);
          if(matrice[x][y]==5)
            {
              matrice[x][y]=2;
              gb.push_back(Macrophage(x,y,dureevie));
               for(int j=0;j<virus.size();j++)
               {
                  if(virus[j].getx()==x && virus[j].gety()==y)
                   {
                      virus.erase (virus.begin()+j);
                    }
                }
              for(int j=0;j<cellule.size();j++)
               {
                  if(cellule[j].getx()==x && cellule[j].gety()==y)
                   {
                      cellule.erase (cellule.begin()+j);
                    }
               }
             }
           else if(matrice[x][y]==0)
            {
              matrice[x][y]=2;
              gb.push_back(Macrophage(x,y,dureevie));
            }
       }
    }
}
int Terrain::getHauteur(){
   return hauteur;
}
int Terrain::getLargeur(){
   return largeur;
}





